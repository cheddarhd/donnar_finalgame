﻿using UnityEngine;

public class GunController : MonoBehaviour
{



    public float damage = 10f;
    public float range = 100f;
    public float impactForce = 30f;
    public float fireRate = 15f;

    public Camera fpsCam;
    public GameObject ImpactEffect;
    public ParticleSystem muzzleFlash;
    private AudioSource mAudio;

    private float nextTimetoFire = 0f;

    void Start()
    {
        mAudio = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimetoFire)
        {
            mAudio.Play();
            nextTimetoFire = Time.time + 1f / fireRate;
            Shoot();
        }

    }
    void Shoot()
    {

        muzzleFlash.Play();
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>(); // finds target script in an object that the gun clicks on
            if (target != null) // to not delete walls and enviroment and only objects with target script attached.
            {
                target.TakeDamage(damage);
            }

            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }

            GameObject ImpactGO = Instantiate(ImpactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(ImpactGO, 2f);
            

        }
    }
}


        
  