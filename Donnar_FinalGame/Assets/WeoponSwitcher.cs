﻿using UnityEngine;

public class WeoponSwitcher : MonoBehaviour
{
    // Start is called before the first frame update

    public int selectedweopon = 0;
    
    void Start()
    {
        SelectWeapon();   
    }

    // Update is called once per frame
    void Update()
    {

        int previousSelectedWeapon = selectedweopon;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (selectedweopon >= transform.childCount - 1)
                selectedweopon = 0;
            else
                selectedweopon++;

        }
        
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (selectedweopon <= 0)
                selectedweopon = transform.childCount - 1;
            else
                selectedweopon--;
        }

        if (previousSelectedWeapon != selectedweopon)
        {
            SelectWeapon();
        }
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach(Transform weapon in transform)
        {
            if (i == selectedweopon)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            i++;
        }
    }
}
